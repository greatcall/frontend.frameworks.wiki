# FrontEnd.Frameworks
Contains customized front-end frameworks such as Bootstrap.

### How do I install it?
###### Small Font (14px)
`install-package GreatCall.FrontEnd.Frameworks.Bootstrap.SmallFont`
###### Medium Font (16px)
`install-package GreatCall.FrontEnd.Frameworks.Bootstrap.MediumFont`
###### Large Font (18px)
`install-package GreatCall.FrontEnd.Frameworks.Bootstrap.LargeFont`

### How do I use it?
View the [wiki](https://bitbucket.org/greatcall/frontend.frameworks.wiki/wiki/).